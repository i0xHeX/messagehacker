package ua.i0xhex.messagehacker.transformer;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import ua.i0xhex.messagehacker.MessageHacker;

public class MessageTransformer {
	public static List<Rule> rules;
	
	public static void init() {
		MessageHacker plugin = MessageHacker.plugin;
		rules = new ArrayList<>();
		String ruleDirectoryName = plugin.getDataFolder().getAbsolutePath() + File.separator + "rules";
		File ruleDirectory = new File(ruleDirectoryName);
		if (!ruleDirectory.isDirectory()) {
			ruleDirectory.mkdirs();
			try {
				File exampleFile = new File(ruleDirectoryName + File.separator + "example.yml");
				InputStream is = MessageTransformer.class.getResourceAsStream("/example.yml");
				FileUtils.copyInputStreamToFile(is, exampleFile);
			} catch (Exception ex) {ex.printStackTrace();}
		}
		scanDirectory(ruleDirectory);
	}
	public static String transform(String json, String plain) {
		String output = null;
		for (Rule rule : rules) {
			output = rule.transform(json, plain);
			if (output != null) return output;
		}
		return null;
	}
	
	private static void scanDirectory(File directory) {
		for (File file : directory.listFiles()) {
			if (file.isFile()) scanFile(file);
			else if (file.isDirectory()) scanDirectory(file);
		}
	}
	private static void scanFile(File file) {
		try {
			if (!file.getName().endsWith(".yml")) return;
			FileConfiguration config = YamlConfiguration.loadConfiguration(file);
			ConfigurationSection section = config.getConfigurationSection(""); if (section == null) return;
			Set<String> keys = section.getKeys(false); if (keys == null) return;
			for (String key : keys) rules.add(new Rule(config, key, file.getName()));
		} catch (Exception ex) {
			Bukkit.getLogger().warning("[MessageHacker] Error occured reading " + file.getName());
			ex.printStackTrace();
		}
	}
}

























