package ua.i0xhex.messagehacker.transformer;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;

import com.comphenix.protocol.wrappers.WrappedChatComponent;

import ua.i0xhex.messagehacker.chat.component.TextComponent;

public class Rule {
	private static final Pattern WORD_PATTERN = Pattern.compile("%word(\\d+)%");
	
	private RuleType ruleType;
	private String input;
	private String output;
	private boolean useInputJson;
	private boolean useOutputJson;
	private boolean useInputColors;
	
	private Pattern inputPattern;
	private ArrayList<Integer> wordsToReplace;
	
	public Rule(FileConfiguration config, String ruleID, String ruleFileName) {
		// Read values
		this.ruleType = RuleType.valueOf(config.getString(ruleID + ".type").toUpperCase());
		this.input = config.getString(ruleID + ".input").replace('&', '§');
		this.output = config.getString(ruleID + ".output").replace('&', '§');
		this.useInputJson = config.getBoolean(ruleID + ".useInputJson", false);
		this.useOutputJson = config.getBoolean(ruleID + ".useOutputJson", false);
		this.useInputColors = config.getBoolean(ruleID + ".useInputColors", true);
		if (this.ruleType == RuleType.REGEX) 
			try {inputPattern = Pattern.compile(this.input);}
			catch (Exception ex) {
				Bukkit.getLogger().warning("[MessageHacker] Error resolving pattern, check for right syntax. Rule: " + ruleFileName + " > " + ruleID);
				this.ruleType = RuleType.NONE;
			}
		if (this.useOutputJson)
			try {WrappedChatComponent.fromJson(this.output);} 
			catch (Exception ex) {
				Bukkit.getLogger().warning("[MessageHacker] Error resolving output json, check for right syntax. Rule: " + ruleFileName + " > " + ruleID);
				this.ruleType = RuleType.NONE;
			}
		
		// Resolve words
		wordsToReplace = new ArrayList<>();
		Matcher matcher = WORD_PATTERN.matcher(this.output);
		while (matcher.find()) wordsToReplace.add(Integer.parseInt(matcher.group(1)));
	}
	
	public String transform(String json, String plain) {
		String message = json;
		if (!this.useInputJson) message = plain;
		if (!this.useInputColors) message = ChatColor.stripColor(message);
		
		String out = null;
		switch (this.ruleType) {
		case NONE:
			return null;
		case EQUALS:
			if (message.equals(this.input)) out = this.output;
			break;
		case STARTS:
			if (message.startsWith(this.input)) out = this.output;
			break;
		case ENDS:
			if (message.endsWith(this.input)) out = this.output;
			break;
		case CONTAINS:
			if (message.contains(this.input)) out = this.output;
			break;
		case REGEX:
			Matcher matcher = inputPattern.matcher(message);
			if (matcher.find()) out = matcher.replaceAll(this.output);
			break;
		}
		
		if (out == null) return null;
		else if (out.isEmpty()) return out;
		else { 
			if (!wordsToReplace.isEmpty()) {
				String[] words = ChatColor.stripColor(plain).split("\\s+");
				for (int wordIndex : wordsToReplace) {
					if (wordIndex > words.length || wordIndex < 1) continue;
					out = out.replace("%word" + wordIndex + "%", words[wordIndex - 1]);
				}
			}
			if (!this.useOutputJson) return TextComponent.fromText(out).toJson();
			else return out;
		}
	}
}

enum RuleType {
	NONE, EQUALS, STARTS, ENDS, CONTAINS, REGEX;
}




























