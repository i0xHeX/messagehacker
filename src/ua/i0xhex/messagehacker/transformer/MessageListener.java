package ua.i0xhex.messagehacker.transformer;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.wrappers.WrappedChatComponent;

import me.clip.placeholderapi.PlaceholderAPI;
import ua.i0xhex.messagehacker.CaptureManager;
import ua.i0xhex.messagehacker.MessageHacker;
import ua.i0xhex.messagehacker.chat.component.TextComponent;

public class MessageListener {
	public MessageListener() {
		ProtocolLibrary.getProtocolManager().addPacketListener(new PacketAdapter(MessageHacker.plugin, PacketType.Play.Server.CHAT) {
			@Override
			public void onPacketSending(PacketEvent e) {
				if (e.getPacket().getChatComponents().getValues().get(0) == null) return;
				Player player = e.getPlayer();
				String playerName = player.getName();
				
				try {
					String inJson = e.getPacket().getChatComponents().getValues().get(0).getJson();
					String inPlain = TextComponent.fromJson(inJson).toText();
					if (inPlain == null) return;
					
					String out = MessageTransformer.transform(inJson, inPlain); 
					if (out != null && !out.isEmpty()) CaptureManager.write(out, playerName);
					else CaptureManager.write(inJson, playerName);
					
					if (out == null) return;
					else if (out.isEmpty()) e.setCancelled(true);
					else {
						if (Bukkit.getServer().getPluginManager().isPluginEnabled("PlaceholderAPI")) 
							out = PlaceholderAPI.setPlaceholders(player, out);
						WrappedChatComponent wcc = WrappedChatComponent.fromJson(out);
						e.getPacket().getChatComponents().write(0, wcc);
					}
				} catch (Exception ex) {
					Bukkit.getLogger().warning("[MessageHacker] Error listening chat packet. Error:");
					ex.printStackTrace();
				}
			}
		});
	}
}





























