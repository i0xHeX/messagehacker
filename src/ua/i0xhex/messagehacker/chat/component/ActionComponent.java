package ua.i0xhex.messagehacker.chat.component;

public class ActionComponent {
	private String action;
	private Object value;
	
	public ActionComponent() {}
	public ActionComponent(String action, String value) {
		this.action = action;
		this.value = value;
	}

	public String getAction() {
		return action;
	}
	public Object getValue() {
		return value;
	}

	public void setAction(String action) {
		this.action = action;
	}
	public void setValue(Object value) {
		this.value = value;
	}
}
