package ua.i0xhex.messagehacker;

import java.io.File;
import java.io.FileWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import ua.i0xhex.messagehacker.chat.component.TextComponent;

public class CaptureManager {
	private static int messagesLeft = 0;
	private static boolean useJson;
	private static String getterName;
	
	private static FileWriter writer;
	
	public static void query(int times, String file, String getter, boolean json) {
		String directory = MessageHacker.plugin.getDataFolder().getAbsolutePath() + File.separator + "captures";
		File captureFolder = new File(directory);
		if (!captureFolder.isDirectory()) captureFolder.mkdirs();
		File captureFile = new File(directory + File.separator + file + ".txt");
		try {
			if (writer != null) writer.close();
			writer = new FileWriter(captureFile, true);
		} catch (Exception ex) {ex.printStackTrace(); return;}
		
		getterName = getter;
		messagesLeft = times;
		useJson = json;
	}
	public static void write(String json, String getter) {
		String message = json;
		if (messagesLeft < 1 || !getter.equals(getterName)) return;
		try {
			if (!useJson) message = TextComponent.fromJson(json).toText();
	        writer.write(getTimeLine(getter) + "\n");
			writer.write(message.replace('§', '&') + "\n\n");
			writer.flush();
			messagesLeft--;
			if (messagesLeft < 1) {
				writer.close();
				writer = null;
			}
		} catch (Exception ex) {ex.printStackTrace();}
	}
	private static String getTimeLine(String getter) {
        Date date = new Date(System.currentTimeMillis());
        DateFormat formatter = new SimpleDateFormat("dd.MM.yyyy > HH:mm:ss");
        String timeLine = "[Catched] " + formatter.format(date) + " > " + getter;
        return timeLine;
	}
}
























